# **Aplicações Distribuídas - INF/UFG - 2017/2**

Este repositório é dedicado para a apresentação do projeto 3 da disciplina de **Aplicações Distribuídas**, ministrada pelo [**Prof. Me. Marcelo Akira**](https://gitlab.com/marceloakira).

Aqui apresentaremos o tutorial sobre o uso de uma tecnologia ou plataforma de Comunicação Indireta. O nosso objetivo é apresentar alguns conceitos teóricos acerca deste tema, e também fazer uma breve introdução na prática.


A tecnologia escolhida para a realização deste tutorial foi o **Redis**.

Estamos todos prontos? Então vamos lá.

---

## **Comunicação Indireta**

#### **Conceito**

A comunicação indireta é definida como a comunicação entre entidades de um sistema distribuído por meio de um intermediário, sem nenhum acoplamento direto entre o remetente e o(s) destinatário(s).

A utilização da comunicação indireta ocorre frequentemente em sistemas distribuídos que são previstas alterações, como por exemplo em ambientes móveis onde o usuário pode se conectar e desconectar rapidamente da rede global. 

Esta comunicação também é bastante utilizada para a disseminação de eventos em sistemas distribuídos, onde o destinatário ou o remetente podem ser desconhecidos.

## **PubSub - Publisher/Subscriber**

O modelo **Publisher Subscriber** é baseado em transmissão de **mensagens assíncronas**. Para isto existe uma ideia de tópicos de mensagens, que representa um mecanismo leve para transmissão de notificações de eventos assíncronos a pontos finais que permitem que os componentes de software se conectem ao tópico para enviar e receber essas mensagens.

Para que a transmissão da mensagem ocorra, um componente chamado **publisher** envia uma mensagem para o tópico. Os tópicos de mensagem transferem imediatamente essas mensagens para todos os **subscribers**.

#### **Identidade não revelada**

O **publisher** não precisa saber quem está usando as informações que estão sendo transmitidas e os **subscribers** também não precisam saber de quem a mensagem vem.

#### **Mas o subscriber irá receber mensagem de todos os publishers?**

Não! Essa que é a parte interessante desta arquitetura. O **subscriber** só irá receber mensagens de tópicos em que ele for assinante.

#### **Arquitetura básica**

![image](https://d1.awsstatic.com/product-marketing/SQS_and_SNS/sns_img_topic.284a4b61940dd8084e25f0fc22c04be40a4acbd1.png)

Como visto na figura acima, temos 2 **publishers** distintos que enviam diferentes mensagens para o tópico. Assim que essas mensagens são recebidas, elas são repassadas para todos os **subscribers** que estão assinando aquele tópico.

## **Redis**

#### **Conceito**
O **Redis** é um armazenamento de estrutura de dados de chave-valor de código aberto e na memória. O Redis oferece um conjunto de estruturas versáteis de dados na memória que permite a fácil criação de várias aplicações personalizadas.

O Redis é extremamente rápido, tanto para escrita como para leitura dos dados, graças ao fato de armazenar seus dados em memória. Apesar disso, o Redis permite que os dados sejam persistidos fisicamente caso desejado.

#### **Benefícios do Redis**

* **Desempenho muito rápido:** Todos os dados do Redis residem na memória principal do seu servidor. Ao eliminar a necessidade de acessar discos, bancos de dados na memória, como o Redis, evitam atrasos de tempo de busca e podem acessar dados com algoritmos mais simples que usam menos instruções de CPU.

* **Estruturas de dados na memória:** O Redis permite que os usuários armazenem chaves que fazem o mapeamento para vários tipos de dados. O tipo de dados fundamental é uma string, que pode ser em formato de dados de texto ou binários e ter no máximo 512 MB.

* **Versatilidade e facilidade de uso:** O Redis é disponibilizado com várias ferramentas que tornam o desenvolvimento e as operações mais rápidas e fáceis, inclusive o PUB/SUB para publicar mensagens nos canais que são entregues para os assinantes.

* **Replicação e persistência:** O Redis emprega uma arquitetura no estilo mestre/subordinado e é compatível com a replicação assíncrona em que os dados podem ser replicados para vários servidores subordinados.

* **Compatibilidade com a sua linguagem de desenvolvimento preferencial:** As linguagens compatíveis incluem Java, Python, PHP, C, C++, C#, JavaScript, Node.js, Ruby, R, Go e muitas outras.


### **Agora que já falamos um pouco sobre a parte mais teórica do tutorial, vamos colocar a mão na massa!**

---

## Instalando o Redis

Este tutorial será baseado no sistema operacional **Mac OSX**.

#### 1º passo: Baixando e instalando o Redis

Primeiramente iremos criar uma pasta chamada **Redis** no diretório dejseado.

Agora iremos realizar o download  da versão mais estável no [**site oficial**](https://redis.io/download) do **Redis**, e salvaremos na pasta criada.

Assim que o download finalizar, abra o terminal da sua máquina, navegue até a pasta **Redis** e execute o seguinte comando: **`tar xzf redis-4.0.6.tar.gz`**. 

Quando o arquivo for descompactado, execute o comando **`cd redis-4.0.6`**, e depois o comando **`make `**. 

Caso você queira testar se a instalação ocorreu corretamente, é necessário somente executar o comando **`make test`** no terminal.

***Pronto, o Redis foi baixado e instalado em sua máquina com sucesso!***

#### 2º Iniciando o servidor Redis

Para iniciarmos o servidor, navegue pelo terminal até a pasta do **Redis**, e então execute o comando **`src/redis-server`**.

![image](imagens/servidor.png)

Se não tiver ocorrido nenhum problema durante a instalação do **Redis**, a mensagem acima será mostrada no seu terminal.

#### 3º Testando comandos 

Novamente é necessário navegar pelo terminal até o diretório do projeto, e agora executar o comando **`src/redis-cli`**.

Lembrando que para este comando dar certo, o servidor deve estar rodando.

![image](imagens/comando1.png)

Pronto, agora já estamos no terminal do servidor e podemos testar o comando **`set foo bar`** para setar a string foo com o valor **"bar"**.

E podemos verificar o valor desta string através do comando **`get foo`**.

![image](imagens/comando2.png)

# **Referências**
* **Sistemas Distribuídos - 5ed: Conceitos e Projeto. Por George Coulouris,Jean Dollimore,Tim Kindberg,Gordon Blair**
* [**Documentação PubSub**](https://aws.amazon.com/pt/pub-sub-messaging/)
* [**Documentação Redis**](https://aws.amazon.com/pt/elasticache/what-is-redis/)
* [**Tutorial Redis**](https://www.youtube.com/watch?v=JGvbEk4jtrU)